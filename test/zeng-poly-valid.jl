using LinearAlgebra
using Polynomials
using IntervalArithmetic
using ValidatedMultipleRoots
using Printf


T = Float64
TT = BigFloat
setprecision(TT, 256)
IT = Interval{T}
ITT = Interval{TT}

l = [4, 3, 2, 1]
k = 4

zz_ = ITT.([1, 2, 3, 4])
zz = IT.(zz_)
z_ = mid.(zz_)
z = T.(z_)

pp_ = polynomial_from_roots(zz_, l)
pp_ /= norm(pp_, 2)
pp = Polynomial(IT.(coeffs(pp_)))
p_ = polynomial_from_roots(z_, l)
p_ /= norm(p_, 2)
p = Polynomial(T.(coeffs(p_)))


function test_valid(N)
    err = fill(Float64[], N, 6)
    for n=1:N
        pn_ = p_ ^ n
        pn = Polynomial(T.(coeffs(pn_)))
        ppn_ = pp_ ^ n
        ppn = Polynomial(IT.(coeffs(ppn_)))
        @show n

        ztilde0 = T[]
        ztilde = T[]
        zz0 = IT[]
        zz = IT[]
        zzbis = IT[]

        try
            @show "err1"
            ztilde0, ll = pejorative_manifold(pn, n*l,
                method="direct")
            err[n,1] = Float64.(abs.((ztilde0-z_) ./ z_))
        catch _ continue end

        try
            @show "err2", ztilde0
            ztilde = pejorative_root(pn, ztilde0, n*l)
            @show "err22"
            err[n,2] = Float64.(abs.((ztilde-z_) ./ z_))
        catch _ continue end

        try
            @show "err3"
            zz0 = pejorative_manifold_validation(ppn, n*l,
                ztilde)
            err[n,3] = Float64.(abs.(diam.(zz0) ./ (2*z_)))
        catch _ zz0 = IT[] end

        try
            @show "err4"
            zz = pejorative_root_validation(ppn, n*l,
                ztilde, zz0, useeigen=true)
            err[n,4] = Float64.(abs.(diam.(zz) ./ (2*z_)))
        catch _ end

        try
            @show "err5"
            zzbis = pejorative_root_validation(ppn, n*l,
                ztilde, zz0, rescale=true, useeigen=true)
            err[n,5] = Float64.(abs.(diam.(zzbis) ./ (2*z_)))
        catch _ end

        try
            @show "err6"
            zzter = pejorative_root_validation(ppn, n*l,
                ztilde, zz0, rescale=true, useeigen=true,
                skipE0=true)
            err[n,6] = Float64.(abs.(diam.(zzter) ./ (2*z_)))
        catch _ end
    end
    return err
end


function printlatex_test_valid(res)
    N = size(res)[1]
    R = (t -> isempty(t) ? NaN : maximum(t)).(res)
    for n=1:N
        @printf "%d \t & \t" n
        @printf "%1.2e \t & \t" R[n,1]
        @printf "%1.2e \t & \t" R[n,2]
        if isnan(R[n,3])
            @printf "---     \t & \t"
        else
            @printf "%1.2e \t & \t" R[n,3]
        end
        if isnan(R[n,6])
            @printf "--- \t"
        elseif isnan(R[n,5])
            @printf "\\textit{(%1.2e)} \t" R[n,6]
        else
            @printf "%1.2e \t" R[n,6]
        end
        @printf "\\\\\n"
    end
end



res = test_valid(20)
printlatex_test_valid(res)
