using Polynomials
using ValidatedMultipleRoots

z_ = Rational{BigInt}.([1, 2, 3, 4])
l_ = [40, 30, 20, 10]
p_ = polynomial_from_roots(z_, l_)

# approximation

T = Float64
p = Polynomial{T}(p_)
z0, l = pejorative_manifold(p, ρ=1e-13)
println(l)
z0, l = pejorative_manifold(p, method="direct", ρ=1e-13)
println(l)
println(abs.(T.(z0-z_)))

z = pejorative_root(p, z0, l)
println(abs.(T.(z-z_)))

# validation

using IntervalArithmetic

IT = Interval{Float64}
pp = Polynomial{IT}(p_)

zz0 = pejorative_manifold_validation(pp, l, z)
println(diam.(zz0))

zz = pejorative_root_validation(pp, l, z, zz0, rescale=true)
println(diam.(zz))
