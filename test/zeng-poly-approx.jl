using LinearAlgebra
using Polynomials
using IntervalArithmetic
using ValidatedMultipleRoots
using Printf


T = Float64
TT = BigFloat
setprecision(TT, 256)

l = [4, 3, 2, 1]
k = 4

z_ = TT.([1, 2, 3, 4])
z = T.(z_)

p_ = polynomial_from_roots(z_, l)
p_ /= norm(p_, 2)
p = Polynomial(T.(coeffs(p_)))

function zeng_poly_approx(n, θ, ρ, ϕ)
    pn_ = p_ ^ n
    pn = Polynomial(T.(coeffs(pn_)))

    k1 = -1
    l1 = Int[]
    relerr1 = NaN
    try
        z1, l1 = pejorative_manifold(pn, θ=θ, ρ=ρ, ϕ=ϕ)
        k1 = length(l1)
        if k1 == k
            relerr1 = maximum(Float64.(abs.((z1-z_) ./ z_)))
        end
    catch _ end

    k2 = -1
    l2 = Int[]
    relerr2 = NaN
    try
        z2, l2 = pejorative_manifold(pn, method="direct",
            θ=θ, ρ=ρ, ϕ=ϕ)
        k2 = length(l2)
        if k2 == k
            relerr2 = maximum(Float64.(abs.((z2-z_) ./ z_)))
        end
    catch _ end

    return k1, l1, relerr1, k2, l2, relerr2
end

function test_zeng_poly_approx(N, θ, ρ, ϕ)
    @printf "Compare Zeng's algorithm pejorative_manifold with our modification for computing multiplicities\n"
    @printf "p_n = (x-1)^(4n)*(x-2)^(3n)*(x-3)^(2n)*(x-4)^n\n"
    @printf "l is the actual multiplicity structure of p_n\n"
    @printf "k1, l1, relerr1 = number of distinct roots, multiplicities and relative error on roots computed by Zeng's algorithm\n"
    @printf "k2, l2, relerr2 idem with our modified routine\n"
    @printf "k1 or k2 < 0 indicates the corresponding routine failed during execution\n"
    @printf "============\n"
    @printf "n\tl\tk1\tl1\trelerr1\t(mult error)\tk2\tl2\trelerr2\t(mult error)\n"

    for n=1:N
        @printf "%d\t" n
        print(n*l)
        k1, l1, relerr1, k2, l2, relerr2 =
            zeng_poly_approx(n, θ, ρ, ϕ)
        @printf "\t%d\t" k1
        print(l1)
        @printf "\t%1.2e\t" relerr1
        if (l1 == n*l)
            @printf " \t"
        else
            @printf "*\t"
        end
        @printf "\t%d\t" k2
        print(l2)
        @printf "\t%1.2e\t" relerr2
        if (l2 == n*l)
            @printf " \n"
        else
            @printf "*\n"
        end
    end
end

test_zeng_poly_approx(70, 1e-13, 1e-13, 100)
