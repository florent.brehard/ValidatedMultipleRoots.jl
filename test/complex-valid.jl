using LinearAlgebra
using Polynomials
using IntervalArithmetic
using ValidatedMultipleRoots
using Printf
using Plots


T = Complex{Float64}
TT = Complex{BigFloat}
setprecision(BigFloat, 256)
IT = Complex{Interval{Float64}}
ITT = Complex{Interval{BigFloat}}

P1 = Polynomial{Rational{BigInt}}([1//1, -1//1, 1//1])
P2 = Polynomial{Rational{BigInt}}([7//1, 4//1, 1//1])
P3 = Polynomial{Rational{BigInt}}([-1//1, -1//1, 1//1])
P4 = Polynomial{Rational{BigInt}}([2//1, 2//1, 1//1])

z_ = vcat([roots(Polynomial{Float64}(Pi))
        for Pi=[P1, P2, P3, P4]]...)
scatter(real.(z_), imag.(z_),
    windowsize=(500,500),
    legend=:none,
    xlim=(-2.2,2.2),
    ylim=(-2.2,2.2),
    axis=:equal,
    xlabel="Re(z)",
    ylabel="Im(z)",
    tickfontsize=11)

function test_valid_complex2(n1, n2, n3, n4)
    l = [n1, n1, n2, n2, n3, n3, n4, n4]
    ppn = Polynomial(Interval{Float64}.(coeffs(
        P1^n1 * P2^n2 * P3^n3 * P4^n4)))
    pn = Polynomial(mid.(coeffs(ppn)))

    ztilde0, ll = pejorative_manifold(pn, l, method="direct")
    @show ll
    err0 = [minimum(Float64.(abs.(z_ .- zt))) for zt=ztilde0]

    ztilde = pejorative_root(pn, ztilde0, ll, verbose=true)

    err1 = [minimum(Float64.(abs.(z_ .- zt))) for zt=ztilde]

    zz0 = Complex{Interval{Float64}}[]
    err2 = NaN
    try
        zz0 = pejorative_manifold_validation(ppn, ll, ztilde0)
        err2 = mag.(zz0-mid.(zz0))
    catch _ end

    err3 = NaN
    try
        zz = pejorative_root_validation(ppn, ll,
        ztilde, zz0, rescale=false, useeigen=true, ϵ=1e-5, skipE0=false)
        err3 = mag.(zz-mid.(zz))
    catch _ end

    return [maximum(err0), maximum(err1), maximum(err2), maximum(err3)]
end

function test_valid_complex2_N(N)
    i = size(N)[1]
    res = fill(0., i, 8)
    res[:,1:4] = N
    for j=1:i
        res[j,5:8] = test_valid_complex2(N[j,:]...)
    end
    return res
end

function print_test_valid_complex2(N, res)
    i = size(N)[1]
    for j=1:i
        @printf "%d, %d, %d, %d \t&\t" N[j, 1] N[j, 2] N[j, 3] N[j, 4]
        @printf "%1.2e \t&\t" res[j, 5]
        @printf "%1.2e \t&\t" res[j, 6]
        if isnan(res[j, 7])
            @printf "--- \t&\t"
        else
            @printf "%1.2e \t&\t" res[j, 7]
        end
        if isnan(res[j, 8])
            @printf "--- \t\\\\\n"
        else
            @printf "%1.2e \t\\\\\n" res[j, 8]
        end
    end
end

N = [2    2    1    1;
     3    1    1    3;
     5    3    3    1;
     6    3    3    1;
     2    5    5    4;
     8    5    2    1;
     8    10   3    7;
     20   11   7    5;
     25   30   3    5;
     25   30   3    10]

res = test_valid_complex2_N(N)

print_test_valid_complex2(N, res)
