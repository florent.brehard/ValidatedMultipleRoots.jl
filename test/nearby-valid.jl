using LinearAlgebra
using Polynomials
using IntervalArithmetic
using ValidatedMultipleRoots
using Printf
using Plots
using Measures


T = Float64
TT = BigFloat
setprecision(TT, 256)
IT = Interval{T}
ITT = Interval{TT}


function test_nearby(ϵ::ITT, m1, m2, m3)
    zz_ = [1-ϵ, 1, -0.5]
    z_ = mid.(zz_)
    l = [m1, m2, m3]
    ll = [m3, m1, m2]
    pp_ = polynomial_from_roots(zz_, l)
    p_ = Polynomial(mid.(coeffs(pp_)))
    pp = Polynomial(IT.(coeffs(pp_)))
    p = Polynomial(T.(coeffs(p_)))

    if m1 == 1 && m2 == 1 && m3 == 1
        ztilde0 = roots(p)
        ztilde = ztilde0[:]
    else
        ztilde0, _ = pejorative_manifold(p, l,
            method="direct")
        ztilde = pejorative_root(p, ztilde0, ll)
    end

    err0 = maximum([minimum(Float64.(abs.(z_ .- zt)))
        for zt=ztilde0])
    err1 = maximum([minimum(Float64.(abs.(z_ .- zt)))
        for zt=ztilde])

    err2 = NaN
    zz0 = IT.([0,0,0])
    try
        zz0 = pejorative_manifold_validation(pp, ll, ztilde)
        err2 = maximum(mag.(zz0-mid.(zz0)))
    catch _ end

    err3 = NaN
    try
        zz = pejorative_root_validation(pp, ll, ztilde, zz0,
            rescale=false, useeigen=true, ϵ=1e-5,
            skipE0=false)
        err3 = maximum(mag.(zz-mid.(zz)))
    catch _ end

    err4 = NaN
    try
        zzx = pejorative_root_validation(pp, ll, ztilde, zz0,
            rescale=false, useeigen=true, ϵ=1e-5,
            skipE0=true)
        err4 = maximum(mag.(zzx-mid.(zzx)))
    catch _ end

    return [err0, err1, err2, err3, err4]
end

ϵs = ITT(1) ./ (10 .^ ITT.(
    [1., 1.5, 2., 2.5, 3., 3.5, 4.]))
transpose(hcat(test_nearby.(ϵs, 20, 20, 5)...))

ϵx = Float64.(mid.(ϵs))
y = hcat(test_nearby.(ϵs, 5, 5, 5)...)

plot(ϵx, y[2,:],
    xaxis=:log,
    xflip=true,
    xticks=[1e-1, 1e-2, 1e-3, 1e-4],
    yticks=[1e-14, 1e-13, 1e-12, 1e-11, 1e-10],
    ylim=(5e-15,1e-10),
    yaxis=:log,
    xlabel="ϵ",
    ylabel="relative accuracy",
    windowsize=(500,400),
    tickfontsize=11,
    legend=:bottomright,
    legendfontsize=11,
    ls=:dash,
    lc=:indigo,
    marker=(:circle, 3, :white),
    markerstrokecolor=:indigo,
    markerstrokewidth=1.5,
    label="")

plot!(ϵx, y[4,:],
    lc=:indigo,
    marker=(:circle, 3, :white),
    markerstrokecolor=:indigo,
    markerstrokewidth=1.5,
    label="5,5,5")

y = hcat(test_nearby.(ϵs, 10, 10, 5)...)

plot!(ϵx, y[2,:],
    ls=:dash,
    lc=:dodgerblue2,
    marker=(:+, 4, :dodgerblue2),
    markerstrokewidth=2,
    label="")

plot!(ϵx, y[4,:],
    lc=:dodgerblue2,
    marker=(:+, 4, :dodgerblue2),
    markerstrokewidth=2,
    label="10,10,5")

y = hcat(test_nearby.(ϵs, 15, 15, 5)...)

plot!(ϵx, y[2,:],
    ls=:dash,
    lc=:green4,
    marker=(:diamond, 3, :white),
    markerstrokecolor=:green4,
    markerstrokewidth=1.5,
    label="")

plot!(ϵx, y[4,:],
    lc=:green4,
    marker=(:diamond, 3, :white),
    markerstrokecolor=:green4,
    markerstrokewidth=1.5,
    label="15,15,5")

y = hcat(test_nearby.(ϵs, 20, 20, 5)...)

plot!(ϵx, y[2,:],
    ls=:dash,
    lc=:orange,
    marker=(:x, 3, :orange),
    markerstrokewidth=2,
    label="")

plot!(ϵx, y[4,:],
    lc=:orange,
    marker=(:x, 3, :orange),
    markerstrokewidth=2,
    label="20,20,5")

y = hcat(test_nearby.(ϵs, 25, 25, 5)...)

plot!(ϵx, y[2,:],
    ls=:dash,
    lc=:red,
    marker=(:square, 2.5, :white),
    markerstrokecolor=:red,
    markerstrokewidth=1.5,
    label="")

plot!(ϵx, y[4,:],
    lc=:red,
    marker=(:square, 2.5, :white),
    markerstrokecolor=:red,
    markerstrokewidth=1.5,
    label="25,25,5")
