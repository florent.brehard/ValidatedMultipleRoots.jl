using LinearAlgebra
using Polynomials
using IntervalArithmetic
using ValidatedMultipleRoots
using Printf
using Plots
using Measures


T = Float64
TT = BigFloat
setprecision(TT, 256)
IT = Interval{T}
ITT = Interval{TT}

l = [4, 3, 2, 1]
k = 4

zz_ = ITT.([1, 2, 3, 4])
zz = IT.(zz_)
z_ = mid.(zz_)
z = T.(z_)

pp_ = polynomial_from_roots(zz_, l)
pp_ /= norm(pp_, 2)
pp = Polynomial(IT.(coeffs(pp_)))
p_ = polynomial_from_roots(z_, l)
p_ /= norm(p_, 2)
p = Polynomial(T.(coeffs(p_)))


function test_perturb_valid1(pnϵ, ppnϵ, n)
    ztilde0, ll = pejorative_manifold(pnϵ, n*l,
        method="direct")
    ztilde = pejorative_root(pnϵ, ztilde0, n*l)
    zz0 = pejorative_manifold_validation(ppnϵ, n*l, ztilde)
    zz = pejorative_root_validation(ppnϵ, n*l,
        ztilde, zz0, rescale=true, useeigen=true)
    return zz
end

# try validation when bits are lost
# in interval representation of the input polynomial
# try for 0 to d lost bits
function test_perturb_valid2(n, d)

    err = fill(NaN, d+1)

    pn_ = p_ ^ n
    pn = Polynomial(T.(coeffs(pn_)))
    ppn_ = pp_ ^ n
    ppn = Polynomial(IT.(coeffs(ppn_)))

    for i = 0:d
        prec = 53 - i
        @show prec
        setprecision(TT, prec)
        pnϵ = Polynomial(T.(TT.(coeffs(pn))))
        ppnϵ = Polynomial(IT.(ITT.(coeffs(ppn))))
        setprecision(TT, 256)
        try
            zzϵ = test_perturb_valid1(pnϵ, ppnϵ, n)
            err[i+1] = maximum(Float64.(abs.(diam.(zzϵ) ./ (2*z_))))
        catch e
            @warn e
        end
    end

    return err
end

nn = 1:5
d = 20
err = hcat([test_perturb_valid2(n, d) for n=nn]...)

xbits = -(0:d) .+ 53

plot(xbits, err[:,1],
    lc=:indigo,
    marker=(:circle, 3, :white),
    markerstrokecolor=:indigo,
    markerstrokewidth=1.5,
    label="m=1",
    # plot style
    windowsize=(550,400),
    right_margin=13mm,
    yaxis=:log,
    xflip=true,
    xticks=[53, 50, 45, 40, 35],
    xlim=(35, 53.5),
    ylim=(1.5e-13, 1.1e-8),
    tickfontsize=11,
    legend=:topleft,
    legendfontsize=11,
    xlabel="relative accuracy of input intervals (in bits)",
    ylabel="relative accuracy of output enclosures")

plot!(twinx(),
    xflip=true,
    yflip=true,
    xlim=(35, 53.5),
    xticks=[],
    ylim=(-log2(1.1e-8), -log2(1.5e-13)),
    tickfontsize=10,
    ylabel="(in bits)",
    )

plot!(xbits, err[:,2],
    lc=:dodgerblue2,
    marker=(:+, 4, :dodgerblue2),
    markerstrokewidth=2,
    label="m=2")

plot!(xbits, err[:,3],
    lc=:green4,
    marker=(:diamond, 3, :white),
    markerstrokecolor=:green4,
    markerstrokewidth=1.5,
    label="m=3")

plot!(xbits, err[:,4],
    lc=:orange,
    marker=(:x, 3, :orange),
    markerstrokewidth=2,
    label="m=4")

plot!(xbits, err[:,5],
    lc=:red,
    marker=(:square, 2.5, :white),
    markerstrokecolor=:red,
    markerstrokewidth=1.5,
    label="m=5")
