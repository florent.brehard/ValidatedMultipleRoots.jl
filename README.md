# ValidatedMultipleRoots.jl

Multiple root finding over dense univariate real or complex polynomials with guaranteed enclosures.

This package implements the algorithms presented in
**[[2]](#BrehardPoteauxSoudant2023)**.

## Installation

```julia
julia> ]
(v1.7) pkg> add https://gitlab.univ-lille.fr/florent.brehard/ValidatedMultipleRoots.jl
```

This package depends on:

* [LinearAlgebra.jl](https://github.com/JuliaLang/julia/tree/master/stdlib/LinearAlgebra) –
Linear algebra routines.

* [Polynomials.jl](https://github.com/JuliaMath/Polynomials.jl) –
Basic arithmetic, integration, differentiation, evaluation, and root finding over dense univariate polynomials.

* [IntervalArithmetic.jl](https://github.com/JuliaIntervals/IntervalArithmetic.jl) –
Validated Numerics in Julia, i.e. rigorous computations with finite-precision floating-point arithmetic.


## New features w.r.t.  [Polynomials.jl](https://github.com/JuliaMath/Polynomials.jl)

The main functions provided by the package
and detailed below are:

* `pejorative_manifold` – Compute numerical approximations
of the distinct roots and recover their multiplicities.
It overloads the corresponding routine in
[Polynomials.jl](https://github.com/JuliaMath/Polynomials.jl)
with the keyword argument `method=direct`
which implements a more direct way to recover multiplicities.

* `pejorative_manifold_validation` – Compute validated enclosures
around the root approximations containing the exact roots.

* `pejorative_root_validation` – Alternative method to compute
validated (often more precise) root enclosures.
Needs initial root enclosures.


## Root approximations and efficient recovering of multiplicities

Given a polynomial $p$ represented by the dense list of its
coefficients `p::Polynomial{T}` (with `T` a
real or complex floating-point type, e.g.,
`Float64` or `Complex{BigFloat}`),

```julia
julia> z, l = pejorative_manifold(p)
```

computes approximate roots `z::Vector{T}`
and multiplicities `l::Vector{Int}` such that
$p \approx p_n \prod_{i=1}^k (x-z_i)^{l_i}$.
The algorithm, described in **[[1]](#Zeng2005)**
and implemented in
[Polynomials.jl](https://github.com/JuliaMath/Polynomials.jl),
computes cofactors $v$ and $w$ such that $w p - v p' = 0$,
and extracts the multiple roots of $p$
as the (simple) roots of the square-free cofactor $v$.

Additional information can be provided by the user, such as
the number `k::Int` of distincts roots,
or the full multiplicity structure `l0::Vector{Int}`:

```julia
julia> z, l = pejorative_manifold(p, k)
julia> z, l = pejorative_manifold(p, l0)
```

The optional keyword argument `method`
selects the multiplicity recovering algorithm:

* `method="iterative"` (default) – the original method of
**[[1]](#Zeng2005)**, implemented in
[Polynomials.jl](https://github.com/JuliaMath/Polynomials.jl).
It is based on repeated *approximate GCD* extraction,
following the square-free decomposition algorithm of $p$.

* `method="direct"` – new method provided by the package.
It is based on the simple observation that the multiplicity
$l_i$ of $z_i$ in $p$ is equal to $\frac{w(z_i)}{v'(z_i)}$.


Other optional arguments include:

* `θ`, `ρ`, `ϕ` – tolerance parameters, see
**[[1]](#Zeng2005)** and explanations in
[Polynomials.jl](https://github.com/JuliaMath/Polynomials.jl/blob/master/src/polynomials/multroot.jl)

* `leastsquares=true` (if `k` or `l0` provided) – compute the
coefficients of cofactors $v, w$ by QR/least-squares
rather than SVD.

* `roundmul=true` (when `method="direct"`) – round the
multiplicities obtained by the above formula
to the nearest integers.

* `cofactors=true` – return the cofactors:

```julia
julia> z, l, v, w = pejorative_manifold(p, cofactors=true)
```

## Validated root enclosures

Given `pp::Polynomial{IT}` (with `IT` a real or complex interval type of
[IntervalArithmetic.jl](https://github.com/JuliaIntervals/IntervalArithmetic.jl),
e.g., `Interval{Float64}` or `Complex{Interval{BigFloat}}`),
the multiplicity structure `l::Vector{Int}`
and root approximations `z::Vector{T}`,
[ValidatedMultipleRoots.jl](https://gitlab.univ-lille.fr/florent.brehard/ValidatedMultipleRoots.jl)
provides two routines to compute rigorous root enclosures
`zz::Vector{IT}`.

The guaranteed property is that for all polynomial $p$
with multiplicity structure $l$ and coefficients contained in
`pp`, there are unique $z_i \in zz_i$ for $1 \leq i \leq k$
such that $p = p_n \prod_{i=1}^k (x-z_i)^{l_i}$.

### Initial root enclosures

The routine `pejorative_manifold_validation`
combines the ideas of `pejorative_manifold`
(compute the cofactors $v, w$, extract the roots $z_i$ of $v$
and recover multiplicities $l_i = \frac{w(z_i)}{v'(z_i)}$)
with a Newton-like fixed-point validation method.

```julia
julia> zz = pejorative_manifold_validation(pp, l, z)
```

Optional arguments are:

* `vw=(v,w)` – provide the (approximate) cofactors $v, w$; otherwise recompute them from the $z_i$.

* `multcheck=false` – skip the multiplicity check

* `ϵ`, `δ` – precision parameters for the Newton-like fixed-point
validation method, see **[[2]](#BrehardPoteauxSoudant2023)**.

### Refined root enclosures

Enclosures computed by `pejorative_manifold_validation`
are sometimes subject to large overestimations due to the
conditioning of the underlying system of equations.
They can be refined by `pejorative_root_validation`,
which is based on the same system of equations as
the approximation routine **[[1]](#Zeng2005)**
`pejorative_root` in
[Polynomials.jl](https://github.com/JuliaMath/Polynomials.jl/blob/master/src/polynomials/multroot.jl).

This routine needs a priori validated enclosures
`zz0::Vector{IT}`, computed by, e.g.,
`pejorative_manifold_validation`.

```julia
julia> zz0 = pejorative_manifold_validation(pp, l, z)
julia> zz = pejorative_root_validation(pp, l, z, zz0)
```

Optional arguments are:

* `rescale=true` – rescale the system of equations to reduce
the wrapping effect during evaluation in interval arithmetic.

* `skipE0=true` – skip the contraction test over the initial
enclosures `zz0`.

* `useeigen=true` – speed up the contraction test over `zz0`
by using a dominant eigenvector. Use `useeigen=false`
if eigenvalue decomposition is not available on the chosen
floating-point type.

* `ϵ`, `δ` – precision parameters for the Newton-like fixed-point
validation method, see
**[[2]](#BrehardPoteauxSoudant2023)**.


## Example

```julia
julia> using Polynomials
julia> using ValidatedMultipleRoots
```

Consider the polynomial
$p = (x-1)^{40} (x-2)^{30} (x-3)^{20} (x-4)^{10}$
approximated to standard IEEE 754 double precision (`Float64`).

```julia
julia> T = Float64
julia> z_ = Rational{BigInt}.([1, 2, 3, 4])
julia> l_ = [40, 30, 20, 10]
julia> p_ = polynomial_from_roots(z_, l_)
julia> p = Polynomial{T}(p_) # floating-point approximation of (x-1)^40*(x-2)^30*(x-3)^20*(x-4)^10
```

The original routine `pejorative_manifold` recovers incorrect
multiplicities, while the new method
(called with `method="direct"`) succeeds.
Then refined approximations are obtained using
`pejorative_root`.

```julia
julia> z0, l = pejorative_manifold(p, ρ=1e-13) # the method of Polynomials.jl
julia> l
[77, 8, 10, 5]

julia> z0, l = pejorative_manifold(p, method="direct", ρ=1e-13) # the new method
julia> l
[40, 30, 20, 10]
julia> abs.(T.(z0-z_)))
[3.976485918322226e-9, 8.2239032583864e-7, 9.349469915598263e-6, 9.999491124013105e-6]

julia> z = pejorative_root(p, z0, l) # refined approximations
julia> abs.(T.(z-z_)))
[2.220446049250313e-16, 2.6645352591003757e-15, 9.769962616701378e-15, 1.2434497875801753e-14]
```

Validated enclosures are obtained using
`pejorative_manifold_validation` followed by
`pejorative_root_validation`.

```julia
julia> using IntervalArithmetic
julia> IT = Interval{Float64}
julia> pp = Polynomial{IT}(p_) # rigorous enclosure of (x-1)^40*(x-2)^30*(x-3)^20*(x-4)^10

julia> zz0 = pejorative_manifold_validation(pp, l, z) # initial enclosures
julia> diam.(zz0)
[5.0138865836935054e-8, 6.3789183908369296e-6, 5.400785199505265e-5, 4.603570462746731e-5]

julia> zz = pejorative_root_validation(pp, l, z, zz0, rescale=true) # refined enclosures
julia> diam.(zz)
[7.549516567451064e-14, 9.243716903029053e-13, 2.9904967391303217e-12, 3.6197711494878604e-12]
```




## Authors <!-- and acknowledgment -->

* [Florent Bréhard](https://pro.univ-lille.fr/florent-brehard) – Université de Lille, CNRS, Centrale Lille, UMR 9189 CRIStAL, F-59000 Lille, France

* [Adrien Poteaux](https://www.fil.univ-lille.fr/~poteaux/) – Université de Lille, CNRS, Centrale Lille, UMR 9189 CRIStAL, F-59000 Lille, France

* Léo Soudant – École normale supérieure Paris-Saclay, Gif-sur-Yvette, France

<!-- ## License
For open source projects, say how it is licensed.
-->

## Project status

This package is a prototype implementation of the algorithms detailed in
**[[2]](#BrehardPoteauxSoudant2023)**.


## Bibliography

**<a name="Zeng2005">[1]</a>**
Zhonggang Zeng, *Computing multiple roots of inexact polynomials*, Math. Comp., 2005, vol. 74, no 250, pp. 869–903.

**<a name="BrehardPoteauxSoudant2023">[2]</a>**
Florent Bréhard, Adrien Poteaux and Léo Soudant, *Validated root enclosures for interval polynomials with multiplicities*, 2023, preprint.
