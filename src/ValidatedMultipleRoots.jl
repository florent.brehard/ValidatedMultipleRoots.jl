module ValidatedMultipleRoots

export pejorative_manifold, pejorative_root, pejorative_manifold_validation, pejorative_root_validation, polynomial_from_roots

using LinearAlgebra
using IntervalArithmetic
using Polynomials

# manipulating real and complex intervals

IntType = Union{IT, Complex{IT}} where IT<:Interval
mkIntType(::Type{T}) where T<:Real = Interval{T}
mkIntType(::Type{Complex{T}}) where T<:Real =
    Complex{Interval{T}}
eltype_IntType(::Type{Interval{T}}) where T<:Real = T
eltype_IntType(::Type{Complex{Interval{T}}}) where T<:Real =
    Complex{T}
expand(x::IT, r) where IT<:Interval = x + IT(-1,1)*r
expand(x::Complex{IT}, r) where IT<:Interval =
    let u = IT(-1,1)*r; x + (u + u*im) end


## approximation routines


# compute initial root approximations with multiplicities
# without any information
# With method = "iterative", use the iterative strategy of Zeng
# to recover the multiplicities associated to the computed roots
# With method = "direct", use the direct method
# based on the cofactors v,w s.t. p = u*v and q = u*w
# if roundmul = true, multiplicities are rounded
# to the closest integer
function pejorative_manifold(
    p::Polynomials.StandardBasisPolynomial{T,X};
    method = "iterative",
    roundmul = true,
    θ = 1e-8,  # zero singular-value threshold
    ρ = 1e-10, # initial residual tolerance
    ϕ = 100,   # residual growth factor
    cofactors=false, # return cofactors v,w?
    )  where {T,X}

    S = float(T)
    u = Polynomials.PnPolynomial{S,X}(S.(coeffs(p)))

    nu₂ = norm(u, 2)
    θ2, ρ2 =  θ * nu₂, ρ * nu₂

    u, v, w, ρⱼ, κ = Polynomials.ngcd(u, derivative(u),
        satol = θ2, srtol = zero(real(T)),
        atol = ρ2, rtol = zero(real(T)))
    ρⱼ /= nu₂

    # root approximations
    zs = roots(v)

    # recover multiplicities
    if method == "iterative"
        ls = pejorative_manifold_iterative_multiplicities(
            u, zs, ρⱼ, θ, ρ, ϕ)
    elseif method == "direct"
        ls = pejorative_manifold_direct_multiplicities(
            v, w, zs, roundmul=roundmul)
    else
        error("pejorative_manifold: unknown multiplicity recovering method: " * string(method))
    end

    if cofactors
        return zs, ls, v, w
    else
        return zs, ls
    end
end


# compute initial root approximations with multiplicities
# when the number k of distinct roots is known
# If method=direct and leastsquares=true, compute the cofactors v,w
# using least-squares rather than Zeng's AGCD refinement strategy
function pejorative_manifold(p::Polynomials.StandardBasisPolynomial{T,X},
    k::Int;
    method = "iterative",
    leastsquares = false,
    roundmul = true,
    θ = 1e-8,  # zero singular-value threshold
    ρ = 1e-10, # initial residual tolerance
    ϕ = 100,   # residual growth factor
    cofactors=false, # return cofactors v,w?
    )  where {T,X}

    S = float(T)
    u = Polynomials.PnPolynomial{S,X}(S.(coeffs(p)))

    nu₂ = norm(u, 2)

    if method == "direct" && leastsquares
        n = degree(u)
        Sy = Polynomials.NGCD.SylvesterMatrix(u, derivative(u), n-k)
        b = Sy[1:end-1,2*k+1] - n * Sy[1:end-1,k] # X^k*p' - n*X^{k-1}*p
        A = Sy[1:end-1,1:end .∉ [[k,2*k+1]]]
        x = zeros(S, 2*k-1)
        Polynomials.NGCD.qrsolve!(x, A, b)
        # w = n*X^{k-1} + ...
        w = Polynomials.PnPolynomial([x[1:k-1]; n])
        # v = X^k + ...
        v = Polynomials.PnPolynomial([-x[k:2*k-1]; 1])

    else
        u, v, w, ρⱼ, κ = Polynomials.ngcd(u, derivative(u),
            degree(u)-k)
        ρⱼ /= nu₂
    end

    # root approximations
    zs = roots(v)

    # recover multiplicities
    if method == "iterative"
        ls = pejorative_manifold_iterative_multiplicities(
            u, zs, ρⱼ, θ, ρ, ϕ)
    elseif method == "direct"
        ls = pejorative_manifold_direct_multiplicities(
            v, w, zs, roundmul=roundmul)
    else
        error("pejorative_manifold: unknown multiplicity recovering method: " * string(method))
    end

    if cofactors
        return zs, ls, v, w
    else
        return zs, ls
    end
end


# compute initial root approximations with multiplicities
# when the multiplicity structure l is known
# If method=direct and leastsquares=true, compute the cofactors v,w
# using least-squares rather than Zeng's AGCD refinement strategy
function pejorative_manifold(p::Polynomials.StandardBasisPolynomial{T,X},
    l::Vector{Int};
    method = "iterative",
    leastsquares = false,
    roundmul = true,
    cofactors=false, # return cofactors v,w?
    )  where {T,X}

    S = float(T)
    u = Polynomials.PnPolynomial{S,X}(S.(coeffs(p)))

    # number of distinct roots
    k = sum(l .> 0)

    if method == "direct" && leastsquares
        n = degree(u)
        Sy = Polynomials.NGCD.SylvesterMatrix(u, derivative(u), n-k)
        b = Sy[1:end-1,2*k+1] - n * Sy[1:end-1,k] # X^k*p' - n*X^{k-1}*p
        A = Sy[1:end-1,1:end .∉ [[k,2*k+1]]]
        x = zeros(S, 2*k-1)
        Polynomials.NGCD.qrsolve!(x, A, b)
        # w = n*X^{k-1} + ...
        w = Polynomials.PnPolynomial([x[1:k-1]; n])
        # v = X^k + ...
        v = Polynomials.PnPolynomial([-x[k:2*k-1]; 1])

    else
        u, v, w, _, _ = Polynomials.ngcd(u, derivative(u),
            degree(u)-k)
    end

    # root approximations
    zs = roots(v)

    # recover multiplicities
    if method == "iterative"
        ls = pejorative_manifold_iterative_multiplicities(
            u, zs, l, roundmul=roundmul)
    elseif method == "direct"
        ls = pejorative_manifold_direct_multiplicities(
            v, w, zs, l, roundmul=roundmul)
    else
        error("pejorative_manifold: unknown multiplicity recovering method: " * string(method))
    end

    if cofactors
        return zs, ls, v, w
    else
        return zs, ls
    end
end


# recover the multiplicity of each root approximation
# using the iterative method of Zeng
# This is exactly pejorative_manifold_multiplicities in Polynomials.jl
function pejorative_manifold_iterative_multiplicities(
    u::Polynomials.PnPolynomial{T},
    zs, ρⱼ, θ, ρ, ϕ) where {T}
    return Polynomials.Multroot.pejorative_manifold_multiplicities(
        u, zs, ρⱼ, θ, ρ, ϕ)
end

# recover the multiplicity of each root approximation
# using the iterative method of Zeng
# but knowing the total multiplicity structure,
# hence the degree of the approximate GCD at each iteration is known
# if roundmul=true, round the floating-point multiplicities
# to the closest integer
function pejorative_manifold_iterative_multiplicities(
    u::Polynomials.PnPolynomial{T},
    zs, l::Vector{Int};
    roundmul = true) where {T}

    ls = ones(Int, length(zs))
    ll = copy(l)

    while !Polynomials.isconstant(u)
        # remove 1 to all multiplicities
        ll .-= 1
        deleteat!(ll, ll .== 0)
        k = length(ll)
        u, v, w, ρⱼ, κ = Polynomials.ngcd(u, derivative(u), degree(u)-k)
        for z ∈ roots(v)
            _, ind = findmin(abs.(zs .- z))
            ls[ind] += 1
        end
    end

    # if roundmul
    #     for i ∈ 1:length(ls)
    #         _, ind = findmin(abs.(l .- ls[i]))
    #         ls[i] = l[ind]
    #     end
    # end

    if roundmul
        ls = Int.(round.(real.(ls)))
    end

    return ls
end


# recover the multiplicity of each root approximation
# directly from the cofactors v,w s.t. p = u*v and q = u*w
# if roundmul=true, round the floating-point multiplicities
# to the closest integer
function pejorative_manifold_direct_multiplicities(
    v::Polynomials.PnPolynomial{T}, w::Polynomials.PnPolynomial{T},
    zs;
    roundmul = true) where {T}

    dv = derivative(v)
    ls = w.(zs) ./ dv.(zs)

    if roundmul
        ls = Int.(round.(real.(ls)))
    end

    return ls
end

# recover the multiplicity of each root approximation
# directly from the cofactors v,w s.t. p = u*v and q = u*w
# if roundmul=true, round the floating-point multiplicities
# to the closest integer
function pejorative_manifold_direct_multiplicities(
    v::Polynomials.PnPolynomial{T}, w::Polynomials.PnPolynomial{T},
    zs, l::Vector{Int};
    roundmul = true) where {T}

    ls = pejorative_manifold_direct_multiplicities(v, w, zs,
        roundmul = false)

    # if roundmul
    #     for i ∈ 1:length(ls)
    #         _, ind = findmin(abs.(l .- ls[i]))
    #         ls[i] = l[ind]
    #     end
    #     ls = Int.(ls)
    # end

    if roundmul
        ls = Int.(round.(real.(ls)))
    end

    return ls
end

# this is Zeng's refinement algorithm, without modification
using Polynomials.Multroot: pejorative_root



## validation routines


# find a vector r of nonnegative radii s.t. E * r + d < r
# E is constant in this linear setting
# solve E * r + dd = r with dd = (1+ϵ) * d .+ δ
function validated_bounds_linear(E, d; ϵ=1e-10, δ=0, maxiter=30)
    T = promote_type(eltype(E), eltype(d))
    IT = Interval{T}
    n = length(d)
    ϵd = ϵ * d .+ δ
    dd = d + ϵd
    # iterate to find the optimal radius
    r = zeros(T, n)
    k = 0
    while k < maxiter
        rr = E * r + dd
        if all(abs.(rr - r) .< ϵd) && all(IT.(E) * IT.(r) + IT.(d) .< r)
            return r
        end
        r = rr
        k = k+1
    end
    error("validated_bounds_linear: convergence to optimal radius failed")
end

# find a vector r of nonnegative radii s.t. E(r) * r + d < r
# E = E(r) is a Lipschitz bound over [x-r,x+r] around the approx x
# note: E(r) may not be constant in the nonlinear setting
# solve E(r) * r + dd = r with dd = (1+ϵ) * d .+ δ
function validated_bounds_nonlinear(E, d; ϵ=1e-10, δ=0, maxiter=30)
    T = eltype(d)
    T = promote_type(eltype(E(T(0)*d)), T)
    IT = Interval{T}
    n = length(d)
    ϵd = ϵ * d .+ δ
    dd = d + ϵd
    # iterate to find the optimal radius
    r = zeros(T, n)
    k = 0
    while k < maxiter
        Er = E(r)
        rr = Er * r + dd
        if all(abs.(rr - r) .< ϵd) && all(IT.(Er) * IT.(r) + IT.(d) .< r)
            return r
        end
        r = rr
        k = k+1
    end
    error("validated_bounds_nonlinear: convergence to optimal radius failed")
end


# compute initial enclosures for the multiple roots of p
# p is given with interval coefficients,
# and the algorithm is correct w.r.t. all polynomials
# represented by p AND with multiplicity structure l,
# cofactors v, w can be provided using vw=(v,w)
# if not specified, they will be recomputed from the roots
# if multcheck=true, check that the validated distinct roots
# have the correct multiplicities
function pejorative_manifold_validation(
    p::Polynomials.StandardBasisPolynomial{T1,X},
    l::Vector{Int},
    z::Vector{T2};
    ϵ=1e-10, δ=0,
    vw=nothing,
    multcheck=true) where {T1<:IntType,T2<:Number,X}

    IT = promote_type(T1, T2)
    T = eltype_IntType(IT)
    pp = Polynomials.PnPolynomial(IT.(p.coeffs))
    p = Polynomials.PnPolynomial(mid.(pp.coeffs))
    z = T.(z)

    n = degree(p)
    k = length(z)

    # reconstruct numerical cofactors v and w
    if vw == nothing
        v = Polynomials.PnPolynomial(reverse(evalG(z, ones(Int, k))))
        w = Polynomials.PnPolynomial([T(0)])
        for i = 1:k
            zi = z[1:k .!= i]
            w += l[i] * Polynomials.PnPolynomial(reverse(evalG(zi,
                ones(Int, k-1))))
        end
    else
        v = Polynomials.PnPolynomial(T.(vw[1].coeffs))
        w = Polynomials.PnPolynomial(T.(vw[2].coeffs))
    end

    x = [coeffs(w)[1:k-1]; coeffs(v)[1:k]; z]

    Spp = Polynomials.NGCD.SylvesterMatrix(pp, derivative(pp), n-k)
    Sp = mid.(Spp)

    # approximate pseudo inverse A of JF(x)
    JFx = evalJF(Sp, n, k, x)
    A = evalJFinv(JFx, n, k)

    # bound the defect
    d = mag.(IT.(A) * evalF(Spp, n, k, IT.(x)))

    # bound Lipschitz matrix
    function E(r)
        xr = expand.(IT.(x), r)
        return mag.(IT(1)I(3*k-1) - IT.(A) * evalJF(Spp, n, k, xr))
    end

    # find validated error bounds for x
    r = validated_bounds_nonlinear(E, d; ϵ=ϵ, δ=δ)
    xx = expand.(IT.(x), r)
    zz = xx[2*k:3*k-1]

    if multcheck # check multiplicities
        ww = Polynomial([xx[1:k-1]; IT(n)])
        vv = Polynomial([xx[k:2*k-1]; IT(1)])
        dvv = derivative(vv)
        mult = real.(ww.(zz) ./ dvv.(zz))
        check = l .∈ mult .&& (l .- 1) .< mult .&& (l .+ 1) .> mult
        if !all(check)
            error("pejorative_manifold_validation: multiplicity check failed")
        end
    end

    return zz
end

# compute refined enclosures for the multiple roots of p
# p is given with interval coefficients,
# and the algorithm is correct w.r.t. all polynomials
# represented by p AND with multiplicity structure l,
# providing zz0 are correct initial enclosures
# for the multiple roots of all such polynomials
# if rescale=true, use a weighted pseudoinverse
# if eigen=true, use dominant eigenvector
# in the proof of contraction on initial enclosures
# if skipE0=true, skip the contraction test over [zz0]
#   => result is no longer rigorous!!
function pejorative_root_validation(
    p::Polynomials.StandardBasisPolynomial{T1,X},
    l::Vector{Int},
    z::Vector{T2},
    zz0::Vector{T3};
    ϵ=1e-10, δ=0,
    rescale=false,
    useeigen=false,
    skipE0=false) where {T1<:IntType,T2<:Number,T3<:IntType,X}

    IT = promote_type(T1, T2, T3)
    T = eltype_IntType(IT)
    pp = Polynomial(IT.(p.coeffs))
    ppc = coeffs(pp)[end-1:-1:1] / coeffs(pp)[end]
    zz = IT.(z)
    zz0 = IT.(zz0)
    n = degree(pp)
    k = length(z)

    # weights if needed
    if rescale
        W = diagm(1 ./ mid.(ppc))
    else
        W = T(1)I(n)
    end

    # approximate inverse
    JG = W * evalJ(T.(z), l)
    A = pinv(JG)

    # defect
    d = mag.(A * (W * (evalG(zz, l)[2:end] - ppc)))

    # bound Lipschitz matrix
    function E(r)
        zr = expand.(zz, r)
        return mag.(I(k) - A * (W * evalJ(zr,l)))
    end

    # refine rigorous enclosure
    r = validated_bounds_nonlinear(E, d; ϵ=ϵ, δ=δ)
    zz = expand.(zz, r)

    # check contraction on union(zz,zz0) (to get uniqueness)
    if skipE0
        return zz # not rigorous!
    else
        zz00 = union.(zz0, zz)
        E0 = mag.(I(k) - A * (W * evalJ(zz00, l)))
        if useeigen
            eigenE0 = eigen(E0)
            i = findmax(abs.(eigenE0.values))[2]
            d0 = abs.(eigenE0.vectors[:,i])
        else
            d0 = d
        end
        validated_bounds_linear(E0, d, ϵ=1e-1, δ=δ)
        return intersect.(zz, zz0)
    end

    # # check enclosure
    # if skipE0 || all(zz .⊆ zz0)
    #     return zz
    # else
    #     error("pejorative_root_validation: computed enclosure not contained in initial enclosure")
    # end
end


## auxiliary routines

# build Πᵢ(X-zᵢ)^lᵢ
# z = vector of roots (of some type T)
# l = vector of corresponding multiplicities (nonnegative integers)
function polynomial_from_roots(z::Vector{T}, l::Vector{Int}) where {T}
    return Polynomial(reverse(evalG(z, l)))
end

using Polynomials.Multroot: evalG!, evalJ!

function evalG(zs::Vector{T}, ls::Vector{Int}) where {T}
    G = zeros(T, sum(ls)+1)
    evalG!(G, zs, ls)
    return G
end

function evalJ(zs::Vector{T}, ls::Vector{Int}) where {T}
    J = zeros(T, sum(ls), length(zs))
    Polynomials.Multroot.evalJ!(J, zs, ls)
    return J
end
# evaluate system F on x
# x[1:k-1] : coeffs of w = n*X^{k-1} + x[k-1]*X^{k-2} + ... + x[1]
# x[k:2*k-1] : coeffs of v = X^k + x[2*k-1]*X^{k-1} + ... + x[k]
# x[2*k:3*k-1] : roots z[1], ..., z[k] of v
# F(x) = y where:
# y[1:n+k-1] : coeffs of w*p-v*p' = y[n+k-1]*X^{n+k-2} + ... + y[1]
# y[n+k:n+2*k-1] : coeffs of
#   (X-z[1]) ... (X-z[k]) = y[n+2*k-1]*X^{k-1} + ... + y[n+k]
# n = degree(p), k = number of distinct roots of p
# Sp = (n-k) Sylvester Matrix of p
function evalF!(y, Sp, n, k, x)
    T = promote_type(eltype(y), eltype(Sp), eltype(x))
    y[1:n+k-1] = (Sp * [x[1:k-1]; T(n); -x[k:2*k-1]; -T(1)])[1:n+k-1]
    y[n+k:n+2*k-1] =
        reverse(evalG(x[2*k:3*k-1], ones(Int, k)))[1:k] - x[k:2*k-1]
    return y
end

function evalF(Sp, n, k, x)
    T = promote_type(eltype(Sp), eltype(x))
    y = zeros(T, n+2*k-1)
    evalF!(y, Sp, n, k, x)
    return y
end

function evalF(p::AbstractPolynomial, k, x)
    n = degree(p)
    Sp = Polynomials.NGCD.SylvesterMatrix(p, derivative(p), n-k)
    return evalF(Sp, n, k, x)
end

# evaluate the Jacobian of F as a (n+2*k-1)x(3*k-1) matrix
function evalJF!(JF, Sp, n, k, x)
    T = promote_type(eltype(JF), eltype(Sp), eltype(x))
    JF .= T(0)
    JF[1:n+k-1, 1:k-1] = Sp[1:n+k-1, 1:k-1]
    JF[1:n+k-1, k:2*k-1] = -Sp[1:n+k-1, k+1:2*k]
    JF[n+k:n+2*k-1, k:2*k-1] = -I(k)
    evalJ!(view(JF, n+k:n+2*k-1, 2*k:3*k-1),
        x[2*k:3*k-1], ones(Int, k))
    JF[n+k:n+2*k-1, 2*k:3*k-1] = JF[n+2*k-1:-1:n+k, 2*k:3*k-1]
    return JF
end

function evalJF(Sp, n, k, x)
    T = promote_type(eltype(Sp), eltype(x))
    JF = zeros(T, n+2*k-1, 3*k-1)
    evalJF!(JF, Sp, n, k, x)
    return JF
end

function evalJF(p::AbstractPolynomial, k, x)
    n = degree(p)
    Sp = Polynomials.NGCD.SylvesterMatrix(p, derivative(p), n-k)
    return evalJF(Sp, n, k, x)
end

function evalJFinv!(JFinv, JF, n, k)
    T = eltype(JF)
    JFinv[1:2*k-1, 1:n+k-1] = pinv(JF[1:n+k-1, 1:2*k-1])
    JFinv[1:2*k-1, n+k:n+2*k-1] .= T(0)
    JFinv[2*k:3*k-1,:] = inv(JF[n+k:n+2*k-1, 2*k:3*k-1]) *
        hcat(JFinv[k:2*k-1,1:n+k-1], T(1)I(k))
    return JFinv
end

function evalJFinv(JF, n, k)
    T = eltype(JF)
    JFinv = zeros(T, 3*k-1, n+2*k-1)
    evalJFinv!(JFinv, JF, n, k)
    return JFinv
end



end # module
